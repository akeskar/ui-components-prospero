- Gaupicker
- JWPlayer
- Iframe
- TextEditorRTE

- All the List stuff

- ListHeader
		- column one (automatic) (is props.title)
		- everything else (starts from right, passed in)
- List
		- consists of ListItem. Each ListItem
				- column one (automatic)
					- icon (may be openable & show children)
					- componentinfo
				- everything else (starts from right, passed in)
					- listitembuttons
					- gradeweight input
					- metrics

		- top special ListItems (passed ins)
				- ReadingQuestionsToggle
				- Introduction

		- placeholder
			- is generated within List

- ListFooter
=======

- may need to write a build script that
	- pushes everything in /lib/media to a CDN
		(for now, use the static directory on prospero.wwnorton.com
		ie https://prospero.wwnorton.com/prospero-instructorview/static/media
		)
	- runs webpack
		(js and css (thru webpack) files will use a CDN_URL env variable)

=======

library:
- List (try to import Preview from react-dnd-multi-backend)
- ListItemComponent
		(include the part with showing/hiding children
		abstract part with connect to redux state
		abtract part with
		)
- ListItem (do not import ListItemButtons - it is modular)
- ListItemComponentInner (straightforward)

no-library:
- ListItemButtons
