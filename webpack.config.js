 var path = require('path');
 var webpack = require('webpack');

 var FILE_LOCATION = process.env.FILE_LOCATION;

 module.exports = {
 	entry: './lib/index.js',
 	output: {
 		path: path.resolve(__dirname, 'build'),
 		filename:'index.js',
 		libraryTarget: 'commonjs2'
 	},
 	module: {
 		rules: [
 			{
        exclude: [
          /\.html$/,
          // We have to write /\.(js|jsx)(\?.*)?$/ rather than just /\.(js|jsx)$/
          // because you might change the hot reloading server from the custom one
          // to Webpack's built-in webpack-dev-server/client?/, which would not
          // get properly excluded by /\.(js|jsx)$/ because of the query string.
          // Webpack 2 fixes this, but for now we include this hack.
          // https://github.com/facebookincubator/create-react-app/issues/1713
          /\.(js|jsx)(\?.*)?$/,
          /\.css$/,
          /\.json$/,
          /\.svg$/
          // /\.ttf(\?.*)?#/,
          // /\.eot(\?.*)?#/,
          // /\.woff(\?.*)?#/
        ],
        use: [
	        {
		        loader: 'url-loader', 
			      options: {
			        limit: 10000,
              emitFile: false,
			        name: FILE_LOCATION + '/[name].[ext]'
			      }	        				
	        }
        ]
 			},
 			{
        test: /\.svg$/,
        use: [
	        {
	        	loader: 'file-loader',
	        	options: {
              emitFile:false,
	        		name: FILE_LOCATION + '/[name].[ext]'
	        	}
	        }
        ]
 			},
 			{
 				test: /\.(js|jsx)$/,
 				use: [
	 				{
	 					loader:'babel-loader',
		        options: {
		          cacheDirectory: true
		        }
	 				}
 				]
 			},
      {
        test: /^((?!\.global).)*css$/,
        use: [
        	{
        		loader: 'style-loader'
        	},
        	{
        		loader: 'css-loader',
        		options: {
        			modules: true,
        			localIdentName: '[local]___[hash:base64:5]',
        			sourceMap:true,
        			minimize:true
        		}
        	},
        	{
        		loader: 'autoprefixer-loader',
        		options: {
			        browsers: [
			          '>1%',
			          'last 4 versions',
			          'Firefox ESR',
			          'not ie < 9', // React doesn't support IE8 anyway
			        ]
        		}
        	}
        ]
      },
      {
        test: /\.global\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: false,
              sourceMap:true,
              minimize:true
            }
          },
          {
            loader: 'autoprefixer-loader',
            options: {
              browsers: [
                '>1%',
                'last 4 versions',
                'Firefox ESR',
                'not ie < 9', // React doesn't support IE8 anyway
              ]
            }
          }          
        ]
      }
 		]
 	},

 	externals: { 
    'react': 'commonjs react'
    // 'moment': 'commonjs moment',
    // 'moment-timezone': 'commonjs moment-timezone'
  }
 
 }