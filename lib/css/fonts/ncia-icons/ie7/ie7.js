/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'ncia-icons\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-gripper-dots': '&#xe90b;',
		'icon-cloud-caution': '&#xe909;',
		'icon-ribbon': '&#xe908;',
		'icon-ebook': '&#xe907;',
		'icon-prospero-owl': '&#xe906;',
		'icon-cogwheels': '&#xe905;',
		'icon-ilg': '&#xe900;',
		'icon-zaps': '&#xe901;',
		'icon-quizmo': '&#xe902;',
		'icon-video-play': '&#xe903;',
		'icon-noqe': '&#xe90a;',
		'icon-calendar': '&#xe904;',
		'icon-fullscreen': '&#xe608;',
		'icon-volume-off': '&#xe611;',
		'icon-volume-on': '&#xe612;',
		'icon-arrowleft': '&#xe800;',
		'icon-arrowright': '&#xe801;',
		'icon-availableoffline': '&#xe802;',
		'icon-exit': '&#xe803;',
		'icon-more': '&#xe804;',
		'icon-menu': '&#xe805;',
		'icon-password': '&#xe806;',
		'icon-circle-info': '&#xe807;',
		'icon-cloud-download': '&#xe808;',
		'icon-search': '&#xe809;',
		'icon-remove-sign': '&#xe80a;',
		'icon-close': '&#xe80b;',
		'icon-book': '&#xe80c;',
		'icon-book-open': '&#xe80d;',
		'icon-bookmark': '&#xe80e;',
		'icon-check': '&#xe80f;',
		'icon-question-sign': '&#xe810;',
		'icon-print': '&#xe811;',
		'icon-pencil': '&#xe812;',
		'icon-seagull': '&#xe813;',
		'icon-unchecked': '&#xe814;',
		'icon-user': '&#xe815;',
		'icon-usergroup': '&#xe816;',
		'icon-education': '&#xe817;',
		'icon-ear': '&#xe818;',
		'icon-chevron-down': '&#xe819;',
		'icon-chevron-up': '&#xe81a;',
		'icon-cogwheel': '&#xe81b;',
		'icon-arrowleft-dlp': '&#xe81c;',
		'icon-video': '&#xe81d;',
		'icon-audio': '&#xe81e;',
		'icon-ilg2': '&#xe81f;',
		'icon-chevron-left': '&#xe820;',
		'icon-chevron-right': '&#xe821;',
		'icon-reading_assignments': '&#xe822;',
		'icon-up-dir': '&#xe823;',
		'icon-down-dir': '&#xe824;',
		'icon-check-no-box': '&#xe825;',
		'icon-pause': '&#xe826;',
		'icon-stop': '&#xe827;',
		'icon-circle': '&#xf111;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
