import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

RadioButton.propTypes = {
	label:PropTypes.string,
	groupName:PropTypes.string,
	value:PropTypes.string,
	checked:PropTypes.bool,
	onChange: PropTypes.func,
	disabled: PropTypes.bool
};

RadioButton.defaultProps = {
	groupName:'Radio Buttons'
}

function RadioButton (props) {
	return (
		<li className={styles.radioButtonListItem}>
			<input 
				aria-labelledby={props.label+'-label'} 
				id={props.label+'-radio'} 
				name={props.groupName}
				role="radio" 
				type="radio" 
				value={props.value}
				checked={props.checked}
				onChange={props.onChange}
				disabled={props.disabled}
			/>
			<label id={props.label+'-label'}>{props.label}</label>
		</li>
	);
}

function RadioButton2 (props) {
	let classNames = styles.radioButton;
	if (props.small) classNames += ' ' + styles.small;
	return (
		<input
			{...props}
			className={classNames}
			type="radio"
			role="radio"
		/>
	);
}

export { RadioButton2 }
export default RadioButton;