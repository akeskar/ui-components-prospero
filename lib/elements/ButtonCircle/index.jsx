import React from 'react';
import styles from './styles.css';
// import Icon from '../Icon/index.jsx';

function ButtonCircle (props) {
	let {
		isAnchor,
		inputRef,
		innerText,
		...buttonProps
	} = props;
	let classNames = styles.buttonCircle;
	if (props.disabled) classNames += ' ' + styles.disabled;
	return (
		<button className={classNames} {...buttonProps}>
			{props.children}
			{innerText}
		</button>
	)
}

export default ButtonCircle;