import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';

import Icon from '../Icon/index.jsx';
import Button from '../Button/index.jsx';

import styles from './styles.css';

class ModalButtonGroup extends Component {
	static get propTypes() {
		return {
			ok: PropTypes.func,
			cancel: PropTypes.func
		}
	}

	componentDidMount() {
		if (this.okBtn) this.okBtn.focus();
	}

	render () {
		return (
			<div>
				{
					this.props.ok ?
					<Button inputRef={(el) => {this.okBtn = el}} action={ this.props.ok } >OK</Button> :
					''
				}
				{
					this.props.cancel ?
					<Button action={ this.props.cancel }>Cancel</Button> :
					''
				}
				{this.props.children}
			</div>
		);
	}
}
class Modal extends Component {
	static get propTypes() {
		return {
			isOpen: PropTypes.bool.isRequired,
			onClose: PropTypes.func,
			title: PropTypes.string,
			zIndex: PropTypes.number.isRequired,
			width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			leftGroup: PropTypes.element,
			rightGroup: PropTypes.element,
			children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element]),

			onAfterOpen: PropTypes.func,
			shouldCloseOnOverlayClick: PropTypes.bool,
			hrStyle: PropTypes.object,
			titleStyle: PropTypes.object,
			headerIcon: PropTypes.element,
			contentStyle: PropTypes.object,
			instructorContent: PropTypes.object
		}
	}
	static get defaultProps() {
		return {
			isOpen: false,
			zIndex: 101,
			width:'80%',
			hrStyle: {
				display:'none'
			}
		}
	}

	constructor (props) {
		super(props);
		this.state = {
			currentSubModal:null
		}
		// this.handleCloseModal = this.handleCloseModal.bind(this);
	}

	render () {
		return (
			<ReactModal
				isOpen={this.props.isOpen}
				onRequestClose={this.props.onClose}
				overlayClassName={styles.modalOverlay}
				className={styles.modalContent}
				style={{
					content: {
						width: this.props.width
					},
					overlay: {
						zIndex: this.props.zIndex
					}
				}}
				contentLabel={this.props.title}
				onAfterOpen={this.props.onAfterOpen}
				shouldCloseOnOverlayClick={this.props.shouldCloseOnOverlayClick}
			>
				<div className={styles.header + ' clearfix'}>
					{this.props.headerIcon}
					<h2 style={this.props.titleStyle} className={styles.title}>{this.props.title}</h2>
					<button className={styles.closeButton} onClick={this.props.onClose}>
						<Icon size={20} fontSize={15} type="close" verticalAlign="middle" innerText="Close Dialog" />
					</button>
				</div>
				<hr style={this.props.hrStyle} />
				{
					this.props.instructorContent
					&&
					<div className={styles.instructor_content}>
						{this.props.instructorContent.icon}
						<div className={styles.instructor_content_header_text}>
							{this.props.instructorContent.fromInstructor}
							<div className={styles.instructor_content_header_subtext}>{this.props.instructorContent.disclaimer}</div>
						</div>
					</div>
				}

				<div style={this.props.contentStyle} className={styles.content}>
					{this.props.children}
				</div>
				{
					(this.props.leftGroup || this.props.rightGroup)
					&&
					<div className={styles.buttons + ' clearfix'}>
						{this.props.leftGroup}
						{this.props.rightGroup}
					</div>
				}

			</ReactModal>
		)
	}
}

export { ModalButtonGroup };
export default Modal;
