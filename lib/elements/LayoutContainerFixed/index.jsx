import React from 'react';
import styles from './styles.css';

function ContainerFixed (props) {
	let editing = ["/", "/edit"].indexOf(props.location.pathname) < 0;
	return (
		<div className={styles.center + ' ' + (editing ? styles.editing : styles.reviewing) }>
			{props.children}
			{ !editing && <div className={styles.push} /> }
		</div>
	);
}

export default ContainerFixed;