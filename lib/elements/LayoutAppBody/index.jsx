import React from 'react';
import styles from './styles.css';


function LayoutAppBody (props) {
	let {
		footer,
		style,
		inputRef,
		...divProps
	} = props;
	let classNames = styles.layoutAppBody;
	if (footer) classNames += ' ' + styles.footer;
	return (
			<div ref={inputRef} style={style} className={classNames} {...divProps} >
				{props.children}
			</div>
	);
}

export default LayoutAppBody;