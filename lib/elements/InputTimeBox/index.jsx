import React, { Component } from 'react';
import PropTypes from 'prop-types'
import styles from './styles.css';

class InputTimeBox extends Component {
	static get propTypes () {
		return {
			value:PropTypes.arrayOf(PropTypes.number),
			doEditData:PropTypes.func,
			editKey:PropTypes.string,
			item:PropTypes.object
		}
	}
	static get defaultProps () {
		return {
			value:[],
			item:{},
			editKey:'timeHHMMSS'
		}
	}
	constructor (props) {
		super(props);
		this.state = {
			hours:  this.props.value[ this.props.value.length - 3],
			minutes:  this.props.value[ this.props.value.length - 2],
			seconds: this.props.value[ this.props.value.length - 1]
		}
		this.onChange = this.onChange.bind(this);
	}
	componentWillReceiveProps (nextProps) {
		if (this.props.value !== nextProps.value) {
			this.setState({
				hours:  nextProps.value[ nextProps.value.length - 3],
				minutes:  nextProps.value[ nextProps.value.length - 2],
				seconds: nextProps.value[ nextProps.value.length - 1]				
			})
		}
	}
	onChange (e) {
		let newValue = [];
		let name = e.currentTarget.name;
		let value = Number(e.currentTarget.value);
		if (name==='hours') {
			this.setState({
				hours:value
			});
			newValue[0] = Number( value );
			newValue[1] = this.state.minutes === undefined ? 0 : Number( this.state.minutes );
			newValue[2] = this.state.seconds === undefined ? 0 : Number( this.state.seconds );
		} 
		else {
			if ( value > 59 ) value = 59;
			if (name === 'minutes') {
				this.setState({
					minutes: value
				});
				newValue[0] = this.state.hours === undefined ? 0 : Number( this.state.hours );
				newValue[1] = Number( value );
				newValue[2] = this.state.seconds === undefined ? 0 : Number( this.state.seconds );				
			}
			else if (name === 'seconds') {
				this.setState({
					seconds:value
				});
				newValue[0] = this.state.hours === undefined ? 0 : Number( this.state.hours );
				newValue[1] = this.state.minutes === undefined ? 0 : Number( this.state.minutes );								
				newValue[2] = Number( value );
			}
		}
		let timeInSeconds = newValue[0] * 60 * 60 + newValue[1] * 60 + newValue[2];

		this.props.doEditData( { _id:this.props.item._id, key:this.props.editKey, value: timeInSeconds }, this.props.item );
	}

	render () {
		let {
			value,
			doEditData,
			key,
			item,
			...outerProps
		} = this.props;
		let { hours, minutes, seconds } = this.state;
		let showHours = '';
		let showMinutes = '';
		let showSeconds = '';
		if ( hours ) showHours = String( hours );
		if ( hours < 10 ) showHours = '0'+hours;
		if ( minutes ) showMinutes = String( minutes );
		if ( minutes < 10 ) showMinutes = '0'+minutes;		
		if ( seconds ) showSeconds = String( seconds );
		if ( seconds < 10 ) showSeconds = '0'+seconds;
		return (
			<div className={styles.inputTimeBox} {...outerProps}>
				<input className={styles.hours} 
					name="hours"
					type="number"
					max="99"
					value={this.state.hours}
					placeholder="HH" 
					maxLength="2" 
					pattern="\d{0,2}"
					onChange={this.onChange}
				/>
				<div className={styles.colon}>:</div>
				<input 
					name="minutes" 
					type="number"
					max="59"
					value={this.state.minutes} 
					className={styles.minutes} 
					placeholder="MM" 
					maxLength="2" 
					pattern="\d{0,2}" 
					onChange={this.onChange}
				/>
				<div className={styles.colon}>:</div>
				<input 
					name="seconds" 
					type="number"
					max="59"
					value={this.state.seconds} 
					className={styles.seconds} 
					placeholder="SS" 
					maxLength="2" 
					pattern="\d{0,2}" 
					onChange={this.onChange}
				/>
			</div>
		)
	}
}


export default InputTimeBox;