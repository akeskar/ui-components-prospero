import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IFrameFunctionalComponent from '../IFrameFunctionalComponent/index.jsx';

const is_ios = (navigator.userAgent || navigator.vendor || window.opera).match(/(iPad|iPhone|iPod)/g) && !window.MSStream ? true : false;

class IFrame extends Component {
	static get propTypes() {
		return {
			component: PropTypes.object,
			postMessage: PropTypes.object,
			action: PropTypes.func,
			src: PropTypes.string,
			type: PropTypes.object,
			height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			show: PropTypes.bool
		}
	}
	static get defaultProps() {
		return {
			show: true,
			component: {},
		};
	}

	componentDidMount () {
		if (this.props.getIframe) {
			this.props.getIframe(this.ifr);
		}

		if (this.props.postMessage) {
			if(this.props.postMessage.onload) {
		 		this.ifr.onload = () => {
	 				this.ifr.contentWindow.postMessage(this.props.postMessage.onload, "*");
		 		}
			}
		}
	}

	// componentWillReceiveProps(nextProps) {
 //    // for (const [objectid, liveData] of Object.entries(nextProps.objectsLive)) {
 //    //   const prevOn = this.props.objectsLive[objectid] ? this.props.objectsLive[objectid].on : null;
 //    //   if (prevOn !== liveData.on) {
 //    //     this.ifr.contentWindow.postMessage({ event: 'onoff', object: objectid, value: liveData.on }, '*');
 //    //   }
 //    // }
 //  }

	shouldComponentUpdate(nextProps, nextState) {
		return false
	}

	render () {
		return (
			<IFrameFunctionalComponent
				height={this.props.height}
				show={this.props.show}
				src={this.props.src || this.props.component.url}
				inputRef={(f) => { this.ifr = f; }}
			/>
		);
	}
}

export default IFrame;
