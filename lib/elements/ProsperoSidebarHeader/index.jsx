import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

ProsperoSidebarHeader.propTypes = {
	nerd:PropTypes.bool,
	style:PropTypes.object
}
function ProsperoSidebarHeader (props) {
	let classNames = styles.prosperoSidebarHeader;
	if (props.nerd) classNames += ' ' + styles.nerd
	return (
		<div className={classNames} style={props.style}>
			{props.children}
		</div>
	)
}

export default ProsperoSidebarHeader;