import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';


import styles from './styles.css';
import { timezones } from './timezones.js';

import Button from '../Button/index.jsx';
import InputSelect from '../InputSelect/index.jsx';
import InputTextBox from '../InputTextBox/index.jsx';
import ToolTip from '../ToolTip/index.jsx';

/*
TO DO: 
make this part of ui-components-prospero
figure out why the error appears when this is loaded from ui-components-prospero
 addComponentAsRefTo(...): Only a ReactOwner can have refs. You might be adding a ref to a component that was not created inside a component's `render` method, or you have multiple copies of React loaded (details: https://fb.me/react-refs-must-have-owner).

*/
GauShow.propTypes = {
	date: PropTypes.string,
	time: PropTypes.string,
	timezone: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
function GauShow (props) {
	let timezone = timezones[Object.keys(timezones)[0]];
	for (const key in timezones) {
		if ( timezones[key].timezone == props.timezone) {
			timezone = timezones[key];
			break;
		}
	}
	if (!props.gau.date) return null;
	return (
		<div className={styles.gauShow}>
			<span>Grades Accepted Until: </span>
			<span>
				{
					moment(props.gau.date + ' ' + props.gau.time).format("MMMM Do YYYY, h:mm:ss a zz") + 
					' ' +
					(timezone.timezone ? timezone.timezone : '')
				}
			</span>
		</div>
	)
}


class GauPicker extends Component {
	static get PropTypes () {
		return {
			date:PropTypes.string,
			time:PropTypes.string,
			timezone:PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			updateGau:PropTypes.func
		}
	}
  constructor (props) {
    super(props)
    this.state = {
      startDate: (this.props.date) ? moment(this.props.date, "MM/DD/YYYY") : "",
      validDate: true
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeRaw = this.handleChangeRaw.bind(this);
    this.handleClearGau = this.handleClearGau.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    let dateString = nextProps.date ? moment(nextProps.date, "MM/DD/YYYY") : "";
    this.setState({
      startDate: (nextProps.date) ? moment(nextProps.date, "MM/DD/YYYY") : "",
      validDate: true,
      value: dateString
    });
  }

  handleChange(date) {
    let dateString = date ? date.format('MM/DD/YYYY') : "";
    this.setState({
      startDate: date,
      validDate: true,
      value: dateString
    });
    this.props.updateGau({ "date": dateString });
    // this.props.dispatch( updateClassActivityData( { key: "grades_accepted_until", value: { "date": dateString } } ) );
  }

  handleChangeRaw(date) {
    let newState = {...this.state};
    newState.validDate = this.validateDate(date);
    if(newState.validDate) {
      newState.startDate = date.length ? moment(date, "MM/DD/YYYY") : "";
      newState.value = null;
      this.props.updateGau({ "date": date.length ? newState.startDate.format('MM/DD/YYYY') : "" });
      // this.props.dispatch( updateClassActivityData({ key: "grades_accepted_until", value: { "date": date.length ? newState.startDate.format('MM/DD/YYYY') : "" } }) );
    } else {
      newState.value = date;
    }
    this.setState(newState);
  }

  handleClearGau() {
    this.setState({
      startDate: "",
      value: "",
      validDate: true
    });
    this.props.updateGau( { date:'', time: '', timezone:'' } );
    // this.props.dispatch( updateClassActivityData( { key: "grades_accepted_until", value: { date:'', time: '', timezone:'' } } ) );
  }

  validateDate(date) {
    var regex = /^(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
    return date ? regex.test(date) : true;
  }

  render() {
    let tooltipStyle = {
      tooltip: {
        width:'260px'
      }
    };

    let picker = (
      <DatePicker
        customInput={<CustomDatePicker />}
        selected={this.state.startDate}
        onChange={this.handleChange}
        isClearable={false}
        onChangeRaw={(event) => this.handleChangeRaw(event.target.value)}
        value={this.state.value}
      />
    );

    return (
      <div className={styles.pickerHolder + ' clearfix'}>
        <div className={styles.labelWrapper}>
          <label>
          Grades Accepted Until:
          </label>
        </div>
        <div className={styles.datePickerWrapper}>
          <ToolTip position="top" styles={tooltipStyle} display={!this.state.validDate} content={"The GAU must be in the format mm/dd/yyyy"}>
            <DatePicker
              customInput={<CustomDatePicker />}
              selected={this.state.startDate}
              onChange={this.handleChange}
              isClearable={false}
              onChangeRaw={(event) => this.handleChangeRaw(event.target.value)}
              value={this.state.value}
            />
          </ToolTip>
        </div>
        <div className={styles.timesWrapper}>
          <Times value={ this.props.time } updateGau={ this.props.updateGau } />
        </div>
        <div className={styles.timezoneWrapper}>
          <Timezones value={ this.props.timezone } updateGau={ this.props.updateGau } />
        </div>
        <div className={styles.clearButtonWrapper}>
          <Button innerText="Clear" action={ this.handleClearGau } />
        </div>
      </div>
    );
  }
}

class CustomDatePicker extends Component {
  render () {
    return (
      <InputTextBox
        onClick={this.props.onClick}
        value={this.props.value}
        placeholder="MM/DD/YYYY"
        iconAfter="calendar"
        onChange={this.props.onChange}
      />
    );
  }
}

function Times (props) {
  let value = props.value ? props.value : ""
  return (
    <InputSelect
      defaultValue=""
      value={ value }
      onChange={
        (e) => {
    			this.props.updateGau( { "time": e.target.value} );
          // props.dispatch( updateClassActivityData({
          //   key:'grades_accepted_until',
          //   value: {
          //     'time': e.target.value
          //   }
          // }) );
        }
      }
    >
      <option value="">Pick a Time</option>
      <option value="6:00">6:00 AM</option>
      <option value="6:30">6:30 AM</option>
      <option value="7:00">7:00 AM</option>
      <option value="7:30">7:30 AM</option>
      <option value="8:00">8:00 AM</option>
      <option value="8:30">8:30 AM</option>
      <option value="9:00">9:00 AM</option>
      <option value="9:30">9:30 AM</option>
      <option value="10:00">10:00 AM</option>
      <option value="10:30">10:30 AM</option>
      <option value="11:00">11:00 AM</option>
      <option value="11:30">11:30 AM</option>
      <option value="12:00">12:00 PM</option>
      <option value="12:30">12:30 PM</option>
      <option value="13:00">1:00 PM</option>
      <option value="13:30">1:30 PM</option>
      <option value="14:00">2:00 PM</option>
      <option value="14:30">2:30 PM</option>
      <option value="15:00">3:00 PM</option>
      <option value="15:30">3:30 PM</option>
      <option value="16:00">4:00 PM</option>
      <option value="16:30">4:30 PM</option>
      <option value="17:00">5:00 PM</option>
      <option value="17:30">5:30 PM</option>
      <option value="18:00">6:00 PM</option>
      <option value="18:30">6:30 PM</option>
      <option value="19:00">7:00 PM</option>
      <option value="19:30">7:30 PM</option>
      <option value="20:00">8:00 PM</option>
      <option value="20:30">8:30 PM</option>
      <option value="21:00">9:00 PM</option>
      <option value="21:30">9:30 PM</option>
      <option value="22:00">10:00 PM</option>
      <option value="22:30">10:30 PM</option>
      <option value="23:00">11:00 PM</option>
      <option value="23:30">11:30 PM</option>
      <option value="23:59">11:59 PM</option>
    </InputSelect>
  );
}
function Timezones (props) {
  // let defaultValue = timezones[moment.tz.guess()] ? timezones[moment.tz.guess()].val+'' : ""
  let value = props.value ? props.value : "";
  return (
    <InputSelect
      defaultValue=""
      value={ value }
      valueOverride={ value }
      onChange={
        (e) => {
      		this.props.updateGau({ "timezone": e.target.value});

          // props.dispatch( updateClassActivityData({
          //   key:'grades_accepted_until',
          //   value: {
          //     'timezone': e.target.value
          //   }
          // }) );
        }
      }
    >
      <option value="">Pick a Timezone</option>
      {
        Object.keys(timezones).map(function (timezoneName, index) {
          let zone = timezones[timezoneName];
          return (
            <option key={timezoneName} value={zone.value}>
            {zone.text || timezoneName}
            </option>
          );
        })
      }
    </InputSelect>
  );
}

export {GauPicker, GauShow};