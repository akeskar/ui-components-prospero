// for consistency with timezone picker in NCIA:
export const timezones = {
  'US/Pacific': { value: 5, timezone: "PST", text:"[GMT-08:00] Pacific Time" },
  'US/Mountain': { value: 9, timezone: "MST", text:"[GMT-07:00] Mountain Time"},
  'US/Central': { value: 11, timezone: "CST", text:"[GMT-06:00] Central Time"},
  'US/Eastern': { value: 15, timezone: "EST", text:"[GMT-05:00] Eastern Time"},

  'US/Hawaii': { value: 3, timezone: "HST", text:"[GMT-10:00] Hawaii"},
  'US/Alaska': { value: 4, timezone: "AKST", text:"[GMT-09:00] Alaska"},
  'US/Arizona': { value: 7, timezone: "MST", text:"[GMT-07:00, no DST] Arizona"},
  'America/Guatemala': { value: 10, timezone: "CST", text:"[GMT-06:00] Central America"},
  'Canada/Saskatchewan': { value: 13, timezone: "CST", text:"[GMT-06:00] Saskatchewan"},
  'Canada/Atlantic': { value: 17, timezone: "AST", text:"[GMT-04:00] Atlantic Time"},
  'Canada/Newfoundland': { value: 21, timezone: "NST", text:"[GMT-03:30] Newfoundland"},

  'Europe/Lisbon': { value: 30, timezone: "WET", text:"[GMT+00:00] Europe - Dublin, Lisbon, London"},
  'Europe/Berlin': { value: 31, timezone: "CET", text:"[GMT+01:00] Europe - Berlin, Rome, Paris"},
  'Europe/Athens': { value: 37, timezone: "EET", text:"[GMT+02:00] Europe - Athens, Istanbul"},
  'Europe/Moscow': { value: 46, timezone: "MSK", text:"[GMT+03:00] Europe - Moscow, St. Petersburg"},

  'Australia/Adelaide': { value: 72, timezone: "ACST", text:"[GMT+09:30] Australia - Adelaide, Darwin"},
  'Australia/Brisbane': { value: 74, timezone: "AEST", text:"[GMT+10:00] Australia - Brisbane"},
  'Australia/Sydney': { value: 75, timezone: "AEST", text:"[GMT+10:00] Australia - Melbourne, Sydney"},
  'Pacific/Auckland': { value: 80, timezone: "NZDT", text:"[GMT+12:00] New Zealand"}
};
