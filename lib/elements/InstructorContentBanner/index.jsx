import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/index.jsx';
import styles from './styles.css';

function InstructorContentBanner (props) {
	return (
    <div className={styles.instructor_content}>
      <Icon type="education" verticalAlign="middle" margin="5px" color="#FFF" fontSize={30} />
      <div className={styles.instructor_content_header_text}>
        FROM YOUR INSTRUCTOR
        <div className={styles.instructor_content_header_subtext}>This is from an outside source and is not created or licensed by W.W. Norton.</div>
      </div>
    </div>
	);
}

export default InstructorContentBanner;