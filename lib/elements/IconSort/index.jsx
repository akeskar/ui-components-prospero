import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

import Icon from '../Icon/index.jsx';

IconSort.propTypes = {
	sortStatus:PropTypes.string,
};
IconSort.defaultProps = {
	sortStatus: null
}
function IconSort (props) {
	return (
		<div className={styles.iconSort}>
			<div>
				<Icon className={props.sortStatus === 'asc' ? styles.black : styles.gray } type="up-dir" size={10} fontSize={11} iconStyle={{float:'right'}} />
				<Icon className={props.sortStatus === 'desc' ? styles.black : styles.gray } type="down-dir" size={10} fontSize={11} iconStyle={{float:'right', clear:'right'}} />
			</div>
		</div>
	);
}

export default IconSort;
