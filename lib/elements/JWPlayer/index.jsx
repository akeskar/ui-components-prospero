import React, { Component } from 'react';

import createEventHandlers from './create-event-handlers';
import getCurriedOnLoad from './helpers/get-curried-on-load';
import getPlayerOpts from './helpers/get-player-opts';
import initialize from './helpers/initialize';
import installPlayerScript from './helpers/install-player-script';
import installLicenseKeyScript from './helpers/install-license-key-script';
import removeJWPlayerInstance from './helpers/remove-jw-player-instance';

import defaultProps from './default-props';
import propTypes from './prop-types';

import './css/norton-skin.global.css';
/***********************************************************
* modified from: https://github.com/micnews/react-jw-player
***********************************************************/

const displayName = 'ReactJWPlayer';

class ReactJWPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adHasPlayed: false,
      hasPlayed: false,
      hasFired: {},
    };
    this.eventHandlers = createEventHandlers(this);
    this.uniqueScriptId = 'jw-player-script';
    this.licenseKeyId = 'jw-player-license-key';
    this._initialize = this._initialize.bind(this);
  }
  componentDidMount() {
    const isJWPlayerScriptLoaded = !!window.jwplayer;
    if (isJWPlayerScriptLoaded) {
      this._initialize();
      return;
    }

    const existingScript = document.getElementById(this.uniqueScriptId);

    if (!existingScript) {
      installPlayerScript({
        context: document,
        onLoadCallback: this._initialize,
        scriptSrc: this.props.playerScript,
        uniqueScriptId: this.uniqueScriptId,
      });
    } else {
      existingScript.onload = getCurriedOnLoad(existingScript, this._initialize);
    }

  }
  shouldComponentUpdate(nextProps) {
    const hasSetupOptionsChanged = this.props.customProps !== nextProps.customProps;

    const hasFileChanged = this.props.file !== nextProps.file;
    const hasPlaylistChanged = this.props.playlist !== nextProps.playlist;

    return hasSetupOptionsChanged || hasFileChanged || hasPlaylistChanged;
  }
  componentDidUpdate() {
    if (window.jwplayer && window.jwplayer(this.props.playerId)) {
      this._initialize();
    }
  }
  componentWillUnmount() {
    removeJWPlayerInstance(this.props.playerId, window);
  }
  _checkLicenseKey(props) {
    // hhl: also add license key
    installLicenseKeyScript({
      context: document,
      licenseKey: this.props.licenseKey,
      uniqueScriptId: this.licenseKeyId,
    });
  }
  _initialize() {
    const component = this;
    const player = window.jwplayer(this.props.playerId);
    const playerOpts = getPlayerOpts(this.props);

    // hhl: also add license key
    const existingLicenseKey = document.getElementById(this.licenseKeyId);
    if (!existingLicenseKey) {
      this._checkLicenseKey();
    }

    initialize({ component, player, playerOpts });
  }
  render() {
    return (
      <div className={this.props.className} id={this.props.playerId} />
    );
  }
}

ReactJWPlayer.defaultProps = defaultProps;
ReactJWPlayer.displayName = displayName;
ReactJWPlayer.propTypes = propTypes;
export default ReactJWPlayer;
