function installLicenseKeyScript({ context, licenseKey, uniqueScriptId }) {
  const jwLicenseKey = context.createElement('script');
  jwLicenseKey.id = uniqueScriptId;
  jwLicenseKey.text = 'jwplayer.key="' + licenseKey + '";';

  context.head.appendChild(jwLicenseKey);
}

export default installLicenseKeyScript;
