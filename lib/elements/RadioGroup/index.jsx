import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

RadioGroup.propTypes = {
	label:PropTypes.string
};
RadioGroup.defaultProps = {
	buttons: []
};

function RadioGroup (props) {
	return (
		<div className={styles.radiogroup}>
			<label id={props.label+'-label'}>{props.label}</label>
			<ul id={props.label+'-radiogroup'} aria-labelledby={props.label+'-label'} role="radiogroup">
				{props.children}
			</ul>
		</div>
	);
}

export default RadioGroup;