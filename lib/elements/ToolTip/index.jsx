import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

/***************************
* loosely based on:
* react-lightweight-tooltip - https://mcumpl.github.io/react-lightweight-tooltip/
* http://www.cssarrowplease.com/
*/

class ToolTip extends Component {

  static get propTypes(){
    return {
    	children: PropTypes.any.isRequired,
      content: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.array,
        ]),
      styles: PropTypes.object,
      position: PropTypes.oneOf(['top', 'bottom', 'left', 'right'])
    }
  }

  static get defaultProps () {
		return {
      content: "Tool Tip!",
      styles: {
        tooltip: {},
        content: {},
        before: {},
        after: {},
        wrapper: {}
      },
      position: 'top'
		}
	}

  constructor(props) {
    super(props);
    this.state = {
      visible: this.props.display !== undefined ? this.props.display : false
    };
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.display !== undefined ? nextProps.display : false
    });
  }

  handleMouseEnter (e) {
    this.setState({
      visible: true
    });
  }

  handleMouseLeave (e) {
    this.setState({
      visible: false
    });
  }

  inlineStyles(position) {
    let inversePosition = {
      top: 'bottom',
      bottom: 'top',
      left: 'right',
      right: 'left'
    }

    return {
      tooltip: {
        position: 'absolute',
        zIndex: '99',
        width: '100%',
        /*minWidth: '200px,'*/
        /*maxWidth: '420px,'*/
        background: '#FFF',
        [inversePosition[position]]: '100%',
        [['top', 'bottom'].indexOf(position) >= 0 ? 'left' : 'top']: '50%',
        ['margin' + this.capitalize(inversePosition[position])]: '10px',
        padding: '5px',
        border: '1px solid #e65100',
        WebkitTransform: 'translate' + (['top', 'bottom'].indexOf(position) >= 0 ? 'X' : 'Y') + '(-50%)',
        msTransform: 'translate' + (['top', 'bottom'].indexOf(position) >= 0 ? 'X' : 'Y') + '(-50%)',
        OTransform: 'translate' + (['top', 'bottom'].indexOf(position) >= 0 ? 'X' : 'Y') + '(-50%)',
        transform: 'translate' + (['top', 'bottom'].indexOf(position) >= 0 ? 'X' : 'Y') + '(-50%)'
      },
      before: {
        [position]: '100%',
      	[['top', 'bottom'].indexOf(position) >= 0 ? 'left' : 'top']: '50%',
        ['margin' + this.capitalize(['top', 'bottom'].indexOf(position) >= 0 ? 'left' : 'top')]: '-7px',
        border: 'solid transparent',
        borderColor: 'rgba(230, 81, 0, 0)',
        ['border' + this.capitalize(position) + 'Color']: '#e65100',
      	borderWidth: '7px',
      	// content: " ",
      	height: '0',
      	width: '0',
      	position: 'absolute',
      	pointerEvents: 'none'
      },
      after: {
        [position]: '100%',
      	[['top', 'bottom'].indexOf(position) >= 0 ? 'left' : 'top']: '50%',
        ['margin' + this.capitalize(['top', 'bottom'].indexOf(position) >= 0 ? 'left' : 'top')]: '-5px',
        border: 'solid transparent',
        borderColor: 'rgba(255, 255, 255, 0)',
        ['border' + this.capitalize(position) + 'Color']: '#FFF',
      	borderWidth: '5px',
      	// content: " ",
      	height: '0',
      	width: '0',
      	position: 'absolute',
      	pointerEvents: 'none'
      }
    }
  }

  capitalize(s) {
      return s && s[0].toUpperCase() + s.slice(1);
  }

  render() {
  	return (
      <div
        onMouseEnter={this.props.display !== undefined ? null : this.handleMouseEnter}
        onMouseLeave={this.props.display !== undefined ? null : this.handleMouseLeave}
        className={styles.wrapper}
        style={this.props.styles.wrapper}
        //ref="wrapper"
      >
        {this.props.children}
        {
          this.state.visible
          &&
          <div style={{...this.inlineStyles(this.props.position).tooltip, ...this.props.styles.tooltip}} className={styles.tooltip}>
            <div style={{...this.inlineStyles(this.props.position).before, ...this.props.styles.before}} />
            <div style={this.props.styles.content} className={styles.content} dangerouslySetInnerHTML={{__html: this.props.content}}></div>
            <div style={{...this.inlineStyles(this.props.position).after, ...this.props.styles.after}} />
          </div>
        }
      </div>
  	);
  }
}

export default ToolTip;
