import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

//should pass in components....
function LayoutCol (props) {
	let style = '';
	if (Array.isArray(props.type)) {
		style = props.type.map( (stylename) => styles[stylename] ).join(' ');
	}
	else
	{
		style = styles[props.type];
	}
	return (
		<div className={style} style={props.style}>
			{props.children}
		</div>
	);
}

LayoutCol.propTypes = {
	type: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired
};

LayoutCol.defaultProps = {
	type: 'col-1'
};

export default LayoutCol;
