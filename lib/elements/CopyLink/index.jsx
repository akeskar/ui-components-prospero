import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/index.jsx';
import styles from './styles.css';

CopyLink.propTypes = {
	buttonText: PropTypes.string,
	url: PropTypes.string,
	openInNewWindow:PropTypes.bool
};
CopyLink.defaultProps = {
	buttonText: "Copy Link",
	url:'',
	openInNewWindow:true
};
function CopyLink ( props ) {
	let { 
		buttonText,
		url,
		openInNewWindow,
		...containerProps
	} = props;
	const linkId = 'urlLink' + Math.floor( Math.random() * 10000 );
	let target = openInNewWindow ? '_new' : '';
	// if (!url) return <span {...containerProps} />;
	if (!url) return null;
	return (
		<span {...containerProps}>
			<a className={styles.prosperoLink} href={props.url} target={target}>
				<input id={linkId} value={props.url} style={{width:props.url.length+'ex'}} readOnly/>
			</a>
			<Button 
				type="tertiary"
				innerText={ buttonText }
				action={ () => {
					document.getElementById( linkId ).select();
					document.execCommand('copy');
				}}
				className={styles.copyLinkButton}
			/>
		</span>
	);
}
export default CopyLink;