import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';


ListHeader.propTypes = {
	title: PropTypes.string,
	styles: PropTypes.object,
	tabButtons: PropTypes.element,
	children: PropTypes.oneOfType( [PropTypes.element, PropTypes.string] )
};
ListHeader.defaultProps = {
	styles:{}
};
/*
to do: aria best practices for table labels
*/
function ListHeader (props) {
	// re do all this
	let title = (<h2 className={styles.title}>{props.title}</h2>);
	if (props.styles.listType === 'overviewstudent') {
		title =(<span className={styles.title}>{props.title}</span>);
	}
	
	return (
		<div className={styles.listHeader + ' ' + styles[props.styles.listType] + ' clearfix'}>
			<div className={styles.leftGroup}>
				{title}
				{props.tabButtons}
			</div>
			<div className={styles.rightGroup}>
				{props.children}
			</div>
		</div>
	);
}

// function GradeWeight (props) {
// 	return (
// 		<div className={styles.rightGroup + ' ' + styles.gradeWeight}>
// 			Grade Weight
// 		</div>
// 	);
// }

export default ListHeader;
