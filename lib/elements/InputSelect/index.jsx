import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css'

import Icon from '../Icon/index.jsx';

InputSelect.propTypes = {
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	items: PropTypes.arrayOf(PropTypes.object),
	large: PropTypes.bool,
	iconAfter: PropTypes.string,
	size:PropTypes.string,
	placeholder: PropTypes.string,
	style: PropTypes.object,
	onChange: PropTypes.func
};

function InputSelect (props) {
	let {
		large,
		charAfter,
		valueOverride,
		onClick,
		items,
		error,
		className,
		...passThru
	} = props;

	let customStyles = '';
	let errorMessage = null;
	if (!props.value) customStyles += ' ' + styles.empty;
	if (large) customStyles += ' ' + styles.large;
	// if (!props.size) customStyles += ' ' + styles.fullWidth;
	if (charAfter) customStyles += ' ' + styles.charAfter;
	if (error) customStyles += ' ' + styles.error;
	if ( styles[className] ) customStyles += ' ' + styles[className];
	else if (className) customStyles+= ' ' + className;

	return (
		<div className={styles.wrapper + customStyles} onClick={onClick}>
			<select
				className={styles.inputSelect + customStyles}
				{...passThru}
			>
				{
					props.items && props.items.length ?
					props.items.map(function (item, index) {
						return (
							<option key={item.value} value={item.value}>
								{item.text}
							</option>
						);
					}) :
					''
				}
				{
					props.children
				}
			</select>
			<div className={styles.iconAfter}><Icon type="down-dir" fontSize={props.large ? 20 : 15} /></div>
		</div>
	);

}

export default InputSelect;
