import React from 'react';
import PropTypes from 'prop-types'
import styles from './styles.css';

import Icon from '../Icon/index.jsx';

InputTextBox.propTypes = {
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	type: PropTypes.string,
	large: PropTypes.bool,
	open: PropTypes.bool,
	iconAfter: PropTypes.string,
	charAfter: PropTypes.string,
	charAfterStyle:PropTypes.string,
	placeholder: PropTypes.string,
	size:PropTypes.string,
	style: PropTypes.object,
	className: PropTypes.string,
	maxLength: PropTypes.number,
	
	onChange: PropTypes.func,
	onKeyDown: PropTypes.func,
	onClick: PropTypes.func,
	onBlur: PropTypes.func
};

InputTextBox.defaultProps = {
	className: '',
	onSubmit: (e) => e.preventDefault()
};

function InputTextBox (props) {
	let {
		// type,
		large,
		open,
		iconAfter,
		charAfter,
		searchIcon,
		onClick,
		onSubmit,
		className,
		inputRef,
		closeButton,
		onButtonClose,
		charAfterStyle,
		children,
		...inputProps
	} = props;
	let customStyles = '';
	if (!props.value) customStyles += ' ' + styles.empty;
	if (props.large) customStyles += ' ' + styles.large;
	if (props.open) customStyles += ' ' + styles.open;
	// if (!props.size) customStyles += ' ' + styles.fullWidth;
	if (props.charAfter) customStyles += ' ' + styles.charAfter;
	if (props.iconAfter) customStyles += ' ' + styles.iconAfter;
	if (styles[props.className]) customStyles += ' ' + styles[props.className];
	else if (props.className) customStyles += ' ' + props.className;

	customStyles += ' ' + styles[props.type];

	let iconSize = 20;
	if (props.large) iconSize = 35;
	if (props.type === 'search') {
		iconSize = 15;
	}

	let closeIconType = 'close';
	let closeIconSize = iconSize;
	if (props.className === 'NERd') {
		closeIconType = 'remove-sign';
		closeIconSize = 14;
	}
	return (
		<form
			className={styles.wrapper + customStyles}
			onClick={ onClick }
			onSubmit={ onSubmit }
			value={props.value}
		>	
			<input 
				ref={inputRef}
				// maxLength={props.maxLength}
				// value={props.value}
				// defaultValue={props.defaultValue}
				className={styles.inputText + customStyles}
				// style={props.style}
				// size={props.size}
				// placeholder={props.placeholder}
				// type={props.type}
				// onChange={props.onChange}
				// onKeyDown={props.onKeyDown}
				// onBlur={props.onBlur}
				{...inputProps}
			/>
			{
				props.children
			}
			{
			props.charAfter ?
			<div className={styles.after} style={{color: charAfterStyle ? charAfterStyle : null }}>{props.charAfter}</div> : ''
			}
			{
			props.iconAfter ?
			<div className={styles.iconOuter}><Icon type={props.iconAfter} fontSize={iconSize} /></div> : ''
			}
			{
			closeButton ?
			<div className={styles.iconOuter} style={{display: props.value ? 'block' : 'none'}}><button value="reset" type="reset" onClick={onButtonClose}><Icon type={closeIconType} fontSize={closeIconSize} style={{verticalAlign:'middle'}} /></button></div> : ''
			}
			{
			props.searchIcon ?
			<div className={styles.iconOuter}><button value="search" type="search" onClick={props.onSubmit}><Icon type={props.searchIcon} fontSize={iconSize} style={{verticalAlign:'middle'}} /></button></div> : ''
			}
			{
			(props.className === 'NERd' && props.type === 'search') ?
			(
				<button className={styles.searchSubmit} aria-label="search" onClick={props.onSubmit}>
					<Icon type="search" size={32} fontSize={14} />
				</button>
			) : ''
			}
		</form>
	);
}

export default InputTextBox;