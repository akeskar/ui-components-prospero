import React from 'react';
import PropTypes from 'prop-types';

ProsperoLogoSVG.propTypes = {
	viewBox: PropTypes.string.isRequired,
	height: PropTypes.number,
	width: PropTypes.number,

	style: PropTypes.object
}
ProsperoLogoSVG.defaultProps = {
	viewBox: '0 0 84 56',
	height:56
}
function ProsperoLogoSVG (props) {
	// let classNames = '';
	let {
		viewBox, 
		height,
		width,
		style,
		className
	} = props;
	width = 84 || width;
	if (height) width = height * 1.5;
	// if (className) classNames = className;
	return (
		<svg 
			xmlns="http://www.w3.org/2000/svg" 
      x="0px"
      y="0px"
			viewBox={viewBox}
			style={style}
      width={`${width}px`}
      height={`${height}px`}
      xmlSpace="preserve"

      className={className}
		>
      <g>
          <path d="M30.975 16.917L43.6 27.415l12.622-10.5v-5.939l4.82-1.885-1.292 7.826h5.13l-1.432-8.876 3.245-1.432L43.6 0 20.5 6.609l10.475 4.366z" transform="translate(-15.609)"/>
          <ellipse cx="3.818" cy="3.818" rx="3.818" ry="3.818" transform="translate(36.792 30.875)"/>
          <ellipse cx="3.818" cy="3.818" rx="3.818" ry="3.818" transform="translate(11.548 30.875)"/>
          <path d="M45.525 87.581l9.759-6.681-14.674 2.577-9.64 8.041 2.768 2.291 6.872-5.726 9.854 8.208-9.854 8.209-25.244-21.023L.716 80.9l9.735 6.681L0 96.29l15.366 12.789 9.64-8.017-2.768-2.291-6.872 5.729-9.854-8.21 9.854-8.208L40.61 109.1 56 96.29z" fill="#d55a1f" transform="translate(0 -61.597)"/>
          <path d="M91.2 181l6.228 9.115 6.251-9.115-6.251-5.2z" fill="#f9a01b" transform="translate(-69.439 -133.854)"/>
      </g>
		</svg>
	);
}

export default ProsperoLogoSVG;


