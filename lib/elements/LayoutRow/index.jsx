import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

//should pass in components....
function LayoutRow (props) {
	let style = '';
	if (Array.isArray(props.type)) {
		style = props.type.map( (stylename) => styles[stylename] ).join(' ');
	}
	else
	{
		style = styles[props.type];
	}

	return (
		<div className={style}>
			{props.children}
		</div>
	);
}

LayoutRow.propTypes = {
	type: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired
};

LayoutRow.defaultProps = {
	type: 'row'
};

export default LayoutRow;
