import React from 'react';
import LayoutAppBody from '../LayoutAppBody/index.jsx';

// import styles from './styles.css';

function NotFound (props) {
	return (
		<LayoutAppBody>
			<div>NOT FOUND</div>
		</LayoutAppBody>
	);
}

export default NotFound;