import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

class Tabs extends Component {
	static get propTypes () {
		return {
			items: PropTypes.array,
			activeIndex: PropTypes.number,
			activeValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool])
		}
	}
	constructor (props) {
		super(props);
		this.state = {
			focusIndex: 0
		}
		this.setFocus = this.setFocus.bind(this);
	}
	tabList = null;
	tabs = null;

	componentDidMount () {
		this.tabs = this.tabList.getElementsByTagName('button');
		if (!this.tabs.length ) this.tabs = this.tabList.getElementsByTagName('a');
		// find the active element
		let focusIndex = null;
		for (var i = 0; i < this.tabs.length; i++) {
			if (focusIndex===null && this.tabs[i].getAttribute('value') === String(this.props.activeValue) ) {
				this.tabs[i].setAttribute('tabIndex', 0);
				focusIndex = i;
			}
			else if (focusIndex===null && i === this.props.activeIndex) {
				this.tabs[i].setAttribute('tabIndex', 0);
				focusIndex = i;
			}
			else if (focusIndex===null && this.tabs[i].classList.contains('navActive')) {
				this.tabs[i].setAttribute('tabIndex', 0);
				focusIndex = i;
			}
 			else {
				this.tabs[i].setAttribute('tabIndex', -1);
			}
		}
		this.setState({
			focusIndex:focusIndex
		});
	}

	setFocus (e) {

		let focusIndex = this.state.focusIndex;
		let itemLength = (this.props.items && this.props.items.length) || (this.props.children && this.props.children.length) || 0;
		if (e.key==='ArrowLeft' || e.keyCode === 37) {
			focusIndex = this.state.focusIndex - 1;
			if (focusIndex < 0) focusIndex = itemLength - 1;
		}
		else if (e.key==='ArrowRight' || e.keyCode === 39) {
			focusIndex = this.state.focusIndex + 1;
			if (focusIndex >= itemLength ) focusIndex = 0;
		}
		this.setState({focusIndex:focusIndex});

		this.tabs[this.state.focusIndex].blur();
		this.tabs[this.state.focusIndex].setAttribute('tabIndex', -1);
		this.tabs[focusIndex].setAttribute('tabIndex', 0);
		this.tabs[focusIndex].focus();

	}
	// componentWillUpdate (nextProps, nextState) {
	// 	if (this.state.focusIndex !== nextState.focusIndex) {
	// 		this.tabs[this.state.focusIndex].blur();
	// 		this.tabs[this.state.focusIndex].setAttribute('tabIndex', -1);
	// 		this.tabs[nextState.focusIndex].setAttribute('tabIndex', 0);
	// 		this.tabs[nextState.focusIndex].focus();
	// 	}
	// }

	render () {
		return (
			<TabList 
				inputRef={(el) => {this.tabList = el;}}
				onKeyDown={this.setFocus}
				focusIndex={this.state.focusIndex}
				{...this.props} 
			/>
		);
	}
}

TabList.propTypes = {
	large: PropTypes.bool,
	inputProps:PropTypes.object,
	inputRef: PropTypes.func,

	items: PropTypes.array,
	activeIndex: PropTypes.number,
	activeValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),

	onItemClick: PropTypes.func,
	onKeyDown: PropTypes.func,

	style: PropTypes.object
};

TabList.defaultProps = {
	focusIndex:0,
	inputProps: {},
	style: {}
}

function TabList (props) {
	let classNames = styles.tabs;
	if (props.large) classNames += ' ' + styles.large + ' large';
	else classNames += ' ' + styles.small + ' small';
	if (props.items) {
		return (
			<ul role="tablist" ref={props.inputRef} className={classNames} style={props.style} {...props.inputProps}>
				{
					props.items.map( (item, index) => {
						let inner = item.text || item.title || item.inner || '';
						let value = item.value;
						let activeValue = props.activeValue;
						if (value === undefined) value = index;
						if (activeValue === undefined) activeValue = props.activeIndex;
						if (props.large) inner = (<h3>{inner}</h3>);
						else inner = (<h4>{inner}</h4>);
						// if props.button
						inner = (
							<button
								className={activeValue === value ? 'navActive' : ''}
								tabIndex={-1}
								value={value}
								onClick={props.onItemClick}
							>
								{inner}
							</button>
						);

						return ( <li onKeyDown={props.onKeyDown} key={'button'+index}>{inner}</li> );
					})
				}
			</ul>
		);
	}
	// old way
	else {
		return (
			<ul role="tablist" ref={props.inputRef} className={classNames} style={props.style} {...props.inputProps}>
				{
					React.Children.map( props.children, (child) => {
						let inner = (<h4>{child}</h4>);
						if (props.large) inner = (<h3>{child}</h3>)
						return (
							<li onKeyDown={props.onKeyDown} className={(child.props['data-hasDivider'] ? styles.divider : '')}>
								{inner}
							</li>
						);
					})
				}
			</ul>
		);
		
	}
}

export default Tabs;