import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IFrameFunctionalComponent from '../IFrameFunctionalComponent/index.jsx';

class IFrameNERD extends Component {
	static get propTypes() {
		return {
			component: PropTypes.object.isRequired,
			nciaUrl: PropTypes.string.isRequired,
			user: PropTypes.object,
			height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			// show: PropTypes.bool.isRequired,

			action: PropTypes.func,
		}
	}
	static get defaultProps () {
		return {
			show:false
		}
	}

	constructor (props) {
		super(props);
		this.state = {
			pageId:'',
			initialNERDUrl: ''
		};
		// this.handleFrameTasks = this.handleFrameTasks.bind(this);
	}

	componentDidMount () {
		let pageId = ''
		let initialNERDUrl = this.props.nciaUrl;

		if (this.props.component.type==='ebook') {
			try {
				if (this.props.component.page_id) {
					pageId = this.props.component.page_id.split(',')[0].trim();
				}
				else if (this.props.component.page_ids) {
					pageId = this.props.component.page_ids.split(',')[0].trim();
				}
				initialNERDUrl = this.props.nciaUrl + this.props.component.short_id + '?manifest_id=' + pageId;
			}
			catch (e) {
				console.log('no page id in ebook component');
			}
		}
		else {
			try {
				// initialNERDUrl = this.props.nciaUrl + this.props.component.short_id + '?manifest_id=' + pageId;
				// temporary:
				initialNERDUrl = this.props.nciaUrl + '1526?prospero';
			}
			catch (e) {
				console.log('no component short_id? cant create initialNERDUrl')
			}
		}

		this.setState({
			pageId:pageId,
			initialNERDUrl: initialNERDUrl
		});

		window.addEventListener('message', this.handleFrameTasks);
		// log into component
		// this.ifr.onload = () => {
		// 	debugger;
		// 	this.ifr.contentWindow.postMessage( {
		// 		type:'login',
		// 		user: this.props.user
		// 	}, '*')
		// }
	}

	componentWillReceiveProps (nextProps) {

		if (nextProps.component.type==='ebook' && this.props.component._id !== nextProps.component._id)
		{
			let pageId = '';
			try {
				if (nextProps.component.page_id) {
					pageId = nextProps.component.page_id.split(',')[0].trim();
				}
				else if (nextProps.component.page_ids) {
					pageId = nextProps.component.page_ids.split(',')[0].trim();
				}
			}
			catch (e) {
				console.log('noo ppage id in ebook component');
			}
			this.setState({
				pageId:pageId
			});

			this.ifr.contentWindow.postMessage( {
				type:'changePageId',
				pageId: pageId,
				nciaUrl: this.props.nciaUrl
			}, '*');
		}
	}

	componentWillUnmount() {
    window.removeEventListener('message', this.handleFrameTasks);
  }

	handleFrameTasks = (e) => {
		console.log('getting messages from frame');
		if (e.data.type === 'ebook.initialized') {
			this.ifr.contentWindow.postMessage( {
				type:'login',
				user: this.props.user
			}, '*');
		}
		// alert('got a message from ncia!w oo');
	}
	render () {
		return (
			<IFrameFunctionalComponent
				height={this.props.height}
				scrollFrame={false}
				show={this.props.component.type === 'ebook'}
				inputRef={(f) => { this.ifr = f; }}
				src={this.state.initialNERDUrl}
			/>
		)
	}
}

export default IFrameNERD;
