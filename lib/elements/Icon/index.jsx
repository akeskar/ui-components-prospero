import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

Icon.propTypes = {
	type: PropTypes.string.isRequired,
	size: PropTypes.number,
	fontSize: PropTypes.number,
	verticalAlign: PropTypes.string,
	innerText: PropTypes.string,
	margin: PropTypes.string,
	iconStyle: PropTypes.object
};
Icon.defaultProps = {
	type:"",
	// size:20,
	fontSize: 30,
	verticalAlign:"baseline",
	innerText:""
	// margin: "0 0 0 0"
};

function Icon (props) {
	let {
		type,
		size,
		fontSize,
		lineHeight,
		verticalAlign,
		color,
		margin,
		iconStyle,
		innerText,
		style,
		className,
		...iconProps
	} = props;
	let classNames = styles['icon-' + props.type];
	if (className) classNames += ' ' + className;

	if (!iconStyle) iconStyle = {};
	if (size) {
		iconStyle.width = size;
		iconStyle.height = size;
		iconStyle.lineHeight = size +'px';
	}
	if (fontSize) iconStyle.fontSize = fontSize + 'px';
	if (lineHeight) iconStyle.lineHeight = lineHeight + 'px';
	if (verticalAlign) iconStyle.verticalAlign = verticalAlign;
	if (color) iconStyle.color = color;
	if (margin) iconStyle.margin = margin;

	style = Object.assign( iconStyle, style )
	// if (iconStyle) style = Object.assign(style, props.iconStyle);

	return (
		<div
			style={style}
			className={classNames}
			aria-hidden="true"
			aria-label={props.type}
			{...iconProps}
		/>
	);
}

export default Icon;
