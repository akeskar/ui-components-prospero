import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

Checkbox.propTypes = {
	large: PropTypes.bool,
	label: PropTypes.string,

	name: PropTypes.string,
	id: PropTypes.string,

	checked: PropTypes.bool,
	
	onChange: PropTypes.func
};
Checkbox.defaultProps = {
	large: false
}
function Checkbox (props) {
	let {
		large,
		label,
		style,
		checkboxStyle,
		...checkBoxProps
	} = props;
	let classNames = styles.checkbox;
	if (props.large) classNames += ' ' + styles.large;
	if (label) {
		label = (
			<label style={{verticalAlign:'middle'}} htmlFor={props.id}>
				{label}
			</label>
		)
	}
	return (
		<span className={classNames} style={style}>
			<input
				// className={classNames}
				name={props.name}
				id={props.id}
				type="checkbox"
				role="checkbox"
				checked={props.checked}
				aria-checked={props.checked}
				// aria-labelledby={'completedLabel'+item._id}
				style={checkboxStyle}
				{...checkBoxProps}
				// style={{marginRight:'8px', opacity:1, position:'relative'}}
				// className={styles.checkboxStyle}
			/>
			{ label }
		</span>
	);
}

export default Checkbox;