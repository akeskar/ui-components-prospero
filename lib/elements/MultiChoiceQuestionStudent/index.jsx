import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../Button/index.jsx';

// import { setDataAndSaveToServer } from '../../actions';

import styles from './styles.css';

class MultiChoiceQuestionStudent extends Component {
	static get propTypes () {
		return {
			component:PropTypes.object,
			submittedResponse: PropTypes.bool,
			currentChoiceIndex: PropTypes.number,
			inputRef: PropTypes.func,
			submitResponse: PropTypes.func
		};
	}
	constructor (props) {
		super(props);
		this.state = {
			submittedResponse:false,
			currentChoiceIndex: null
		};
		this.setCurrentChoiceIndex = this.setCurrentChoiceIndex.bind(this);
		this.submitResponse = this.submitResponse.bind(this);
	}
	componentWillMount () {
		if (this.props.submittedResponse !== undefined) {
			this.setState({
				submittedResponse:this.props.submittedResponse
			});
		}
		if (this.props.currentChoiceIndex !== undefined ) {
			this.setState({
				currentChoiceIndex: this.props.currentChoiceIndex
			});
		}
	}
	componentWillReceiveProps (nextProps) {
		if (nextProps.submittedResponse !== this.props.submittedResponse) {
			this.setState({
				submittedResponse: nextProps.submittedResponse
			});
		}
		if (nextProps.currentChoiceIndex !== this.props.currentChoiceIndex) {
			this.setState({
				currentChoiceIndex:nextProps.currentChoiceIndex
			});
		}
	}

	setCurrentChoiceIndex (e) {
		// if (this.state.submittedResponse !== null) return;
		var index = e && e.currentTarget && e.currentTarget.value;
		// console.log('setCurrentChoiceIndex', index);
		this.setState({
			currentChoiceIndex: index
		});
	}

	submitResponse () {
		this.setState({
			submittedResponse:true
		});
		let grade = 0;
		// if it's correct
		if (this.props.component.correct_answer === parseInt(this.state.currentChoiceIndex, 10)) {
			grade = 100;
		}
		let response = {
			_id: this.props.component._id,
			gradeable:true,
			data: {
				grade:grade,
				completed: true,
				activity_data:[{
					crud: 'update',
					key: 'submittedResponseIndex',
					value: parseInt(this.state.currentChoiceIndex, 10)
				}],
				record:{
					// what should be in here?
					grade:grade,
					submitted_grade: grade,
					submission_source: "prospero"
				}
			}
		};
		if (this.props.submitResponse) {
			this.props.submitResponse( response )
		}
	}

	render () {
		return (
			<InstructorQuestionView
				question={this.props.component}
				currentChoiceIndex={this.state.currentChoiceIndex}
				setCurrentChoiceIndex={this.setCurrentChoiceIndex}
				submittedResponse={this.state.submittedResponse}
				submitResponse={this.submitResponse}
			/>
		);
	}
}

InstructorQuestionView.propTypes = {
	question: PropTypes.object,
	userChoices: PropTypes.array
};
InstructorQuestionView.defaultProps = {
	question: {}
};
function InstructorQuestionView (props) {
	let classNames = styles.instructorContent + ' instructorContent';
	let choices = props.question.choices || [];
	let submitAnswer = (
		<div className={styles.submitAnswer}>
			<Button
				type="primary"
				disabled={props.currentChoiceIndex === null || props.submittedResponse}
				innerText="Submit Answer"
				action={props.submitResponse}
			/>
			<p>You can only submit an answer once.</p>
		</div>
	)

	if (props.submittedResponse) {
		classNames += ' ' + styles.submittedResponse;

		let grade = 0;
		if (props.question.correct_answer === parseInt(props.currentChoiceIndex, 10)) grade = 100;
		submitAnswer = (
			<div className={styles.submitAnswer}>
				<Button
					type="tertiary"
					disabled={true}
					innerText="Answer Submitted"
				/>
				<p>Grade: {grade}%</p>
			</div>
		)
	}

	if (!choices.length) submitAnswer = null;

	return (
		<div className={classNames} ref={props.inputRef} >
			<h1 className={styles.title + ' class2'}>{props.question.title}</h1>
			<div className={styles.textContent} dangerouslySetInnerHTML={{__html: props.question.question_text }} />
			<div className={styles.answers_holder}>
			{
				choices.map( (choice, index) => {
					return (
						<QuestionChoice
							key={props.question._id + index}
							choice={choice}
							choiceIndex={index}
							onClick={props.setCurrentChoiceIndex}
							selected={index == props.currentChoiceIndex }
							correct={index == props.question.correct_answer }
							submittedResponse={props.submittedResponse}
						/>
					)
				})
			}
			</div>
			{ submitAnswer }
		</div>
	)
}

function QuestionChoice (props) {
	let choiceLetter = String.fromCharCode(65 + props.choiceIndex);
	let feedbackLabel = null;
	let classNames = styles.mc_choice_outer;
	if (props.selected) {
		if (props.submittedResponse && props.correct) {
			feedbackLabel=(<div className={styles.feedbackLabel}>CORRECT</div>);
			classNames += ' ' + styles.correct;
		}
		else if (props.submittedResponse && !props.correct) {
			feedbackLabel=(<div className={styles.feedbackLabel}>INCORRECT</div>);
			classNames += ' ' + styles.incorrect;
		}
		else {
			classNames += ' ' + styles.selected;
		}
	}
	return (
		<div className={classNames}>
			{ feedbackLabel }
			<button
				disabled={props.submittedResponse}
				value={props.choiceIndex}
				onClick={props.onClick}
				className={styles.mc_choice + ' clearfix'}
			>
				<div className={styles.mc_choice_marker}>{ choiceLetter }</div>
				<div className={styles.mc_choice_text}>
					{props.choice.text}
				</div>
			</button>
		</div>
	)
}


export default MultiChoiceQuestionStudent;
