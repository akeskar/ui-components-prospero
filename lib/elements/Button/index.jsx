import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';
// const styles = {};

Button.propTypes = {
	name: PropTypes.string,
	type: PropTypes.string,
	innerText: PropTypes.oneOfType([PropTypes.string, PropTypes.element ]),
	action: PropTypes.func,
	data: PropTypes.object,
	icon: PropTypes.element,
	onMouseOver: PropTypes.func,
	onMouseOut: PropTypes.func,
	active: PropTypes.bool,
	activeClassName: PropTypes.string,
	inputRef: PropTypes.func,
	style: PropTypes.object,
	isAnchor: PropTypes.bool,
	href: PropTypes.string
	// value: PropTypes.bool
};

Button.defaultProps = {
	activeClassName: styles.active,
	name: null,
	innerText: '',
	type: 'secondary'
};

function Button (props) {
	let {
		isAnchor,
		isWrapper,
		className,
		type,
		action,
		active,
		activeClassName,
		inputProps,
		inputRef,
		innerText,
		icon,
		...passThru
	} = props;

	let classNames = className || '';
	if (styles[className]) classNames = styles[className];
	if (styles[type]) classNames += ' ' + styles[type];

	if (!inputProps) inputProps = {};
	if ( passThru.disabled || inputProps.disabled ) classNames += ' ' + styles.disabled;
	if ( active ) classNames += ' ' + styles[activeClassName];
	// if ( isWrapper ) classNames += ' ' + styles.btn;
	// let useProps = {
	// 	...inputProps,
	// 	...passThru,
	// 	ref: inputRef,
	// 	className:classNames
	// };
	if (isAnchor) {
		return (
			<a {...passThru} {...inputProps} onClick={action} ref={inputRef} className={classNames} >
				
				{props.children}
				{innerText}
				{icon}
				
			</a>
		)
	}
	else if (isWrapper) {

		if ( React.Children.count(props.children) ) {
			let { 
				children,
				...passThruButNoChildren
			} = passThru;
			let childprops = {
				...passThruButNoChildren,
				...inputProps,
				onClick:action,
				ref:inputRef,
				className: classNames
			};
			let childWithProps = React.cloneElement( props.children, childprops );
			// console.log(childWithProps);
			return childWithProps;	
		}
		// return (
		// 	<div {...passThru} {...inputProps} onClick={action} ref={inputRef} className={classNames}>
		// 		{props.children}
		// 	</div>
		// )
	} 
	else {
		return (
			<button {...passThru} {...inputProps} onClick={action} ref={inputRef} className={classNames} >
				
				{props.children}
				{innerText}
				{icon}
				
			</button>
		);
	}
}

export function ButtonStudent (props) {
	return (
		<Button className={styles.student} {...props} />
	)
}

export default Button;
