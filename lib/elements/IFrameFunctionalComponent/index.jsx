import React, { Component } from 'react';
import PropTypes from 'prop-types';

const is_ios = (navigator.userAgent || navigator.vendor || window.opera).match(/(iPad|iPhone|iPod)/g) && !window.MSStream ? true : false;

IFrameFunctionalComponent.propTypes = {
	height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	show: PropTypes.bool,
	scrollFrame: PropTypes.bool,
	inputRef: PropTypes.func
};
IFrameFunctionalComponent.defaultProps = {
	height: '100%',
	scrollFrame: true

};
function IFrameFunctionalComponent (props) {
	let iframe = (
		<iframe
      style={{
      	width: '100%',
      	height: props.height,
      	display: props.show ? 'block' : 'none'
      }}
      src={props.src}
      ref={props.inputRef}
		/>
	);
	// inputRef==={(f) => { this.ifr = f; }}
	if (is_ios && props.scrollFrame)
	{
		return (
			<div id="scroll_frame"
			display={ (props.show ? 'block' : 'none') }
			style={{
				'overflow':'auto',
				'-webkit-overflow-scrolling':'touch',
				'width':'100%',
				'height': props.height,
				'display': props.show ? 'block' : 'none'
			}}>
				{iframe}
			</div>
		)
	}
	else
	{
		return iframe;
	}
}

export default IFrameFunctionalComponent;
