import React from 'react';
import PropTypes from 'prop-types';

const ICONS = {
  alert : 'M36.845,32.268L25,11.751c-0.55-0.953-1.45-0.953-2,0L11.155,32.268c-0.55,0.953-0.1,1.732,1,1.732h23.691 C36.945,34,37.395,33.221,36.845,32.268z M26,31.5c0,0.275-0.225,0.5-0.5,0.5h-3c-0.275,0-0.5-0.225-0.5-0.5v-3 c0-0.275,0.225-0.5,0.5-0.5h3c0.275,0,0.5,0.225,0.5,0.5V31.5z M26,22.5c0,0.275-0.044,0.721-0.098,0.99l-0.9,2.77 C24.949,26.529,24.68,27,24.404,27h-0.785c-0.275,0-0.545-0.471-0.6-0.74l-0.92-2.645c-0.055-0.27-0.1-0.84-0.1-1.115v-4 c0-0.275,0.225-0.5,0.5-0.5h3c0.275,0,0.5,0.225,0.5,0.5V22.5z',
  calendar : 'M.5 7v16a1 1 0 0 0 1 1h21a1 1 0 0 0 1-1V7zm5 15h-3v-3h3zm0-4h-3v-3h3zm0-4h-3v-3h3zm4 8h-3v-3h3zm0-4h-3v-3h3zm0-4h-3v-3h3zm4 8h-3v-3h3zm0-4h-3v-3h3zm0-4h-3v-3h3zm4 8h-3v-3h3zm0-4h-3v-3h3zm0-4h-3v-3h3zm4 8h-3v-3h3zm0-4h-3v-3h3zm0-4h-3v-3h3zm2-11v3H.5V3a1 1 0 0 1 1-1h2V1a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1h11V1a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1h2a1 1 0 0 1 1 1z',
  clock : 'm9,0.1a8.9,8.9 0 1 0 8.9,8.9a8.9,8.9 0 0 0 -8.9,-8.9zm0,15.575a6.675,6.675 0 1 1 6.675,-6.675a6.675,6.675 0 0 1 -6.675,6.675zm0.74167,-7.95067l0,-3.53033a0.39308,0.39308 0 0 0 -0.38567,-0.38567l-0.74167,0a0.38567,0.38567 0 0 0 -0.37083,0.38567l0,3.53033a1.41658,1.41658 0 0 0 -0.534,0.534l-2.047,0a0.4005,0.4005 0 0 0 -0.37083,0.40792l0,0.70458a0.37083,0.37083 0 0 0 0.37083,0.37083l2.06183,0a1.48333,1.48333 0 0 0 1.27567,0.74167a1.48333,1.48333 0 0 0 1.48333,-1.48333a1.48333,1.48333 0 0 0 -0.74167,-1.27567z',
  ebook : 'M19 23.5c0 .4-.1.5-.5.5H3.6a.909.909 0 0 1-.8-.4L.4 20.9a.758.758 0 0 1-.2-.4.4.4 0 0 1-.2-.4V.5C0 .1.2 0 .5 0h14.6c.3 0 .5.1.5.5v19.6a.472.472 0 0 1-.5.5H1.7l.8.9h13.8c.1 0 .2 0 .2-.2V1.2a.265.265 0 0 1 .3-.3h.3c.1 0 .2.1.2.3v20.6c0 .3-.1.5-.4.5H3.3l.7.9h14c.1 0 .2 0 .2-.2V2.9c0-.2.1-.3.2-.3h.4c.1 0 .2.1.2.3v20.6zm-6-10.7h-2.7v1.7H6.2v-3.1H13V7.2c0-1.4-.4-2.2-1.1-2.5a5.222 5.222 0 0 0-2.1-.4H6.6c-.6 0-1.1.1-1.5.1a2.372 2.372 0 0 0-1.2.9 2.421 2.421 0 0 0-.4 1.3c0 .4-.1.9-.1 1.3v5.4a6.66 6.66 0 0 0 .1 1.6 1.217 1.217 0 0 0 .6 1 1.644 1.644 0 0 0 1.2.8 5.739 5.739 0 0 0 1.2.1h3.4c1.4 0 2.2-.3 2.5-.9a6.356 6.356 0 0 0 .6-1.8zm-2.7-6.1v2.7H6.2V6.7z',
  expand : 'M8 11L4 5.5 0 0h16l-4 5.5z',
  warning: 'M9 0a9 9 0 1 0 9 9 9.036 9.036 0 0 0-9-9zm1.475 14.607a.348.348 0 0 1-.369.369H7.893a.348.348 0 0 1-.369-.369v-2.214a.348.348 0 0 1 .369-.369h2.213a.348.348 0 0 1 .369.369zm0-8.115a2.51 2.51 0 0 1-.074.738l-.663 3.54c-.074.221-.221.516-.443.516H8.7c-.221 0-.369-.369-.443-.516L7.6 7.377a3.026 3.026 0 0 1-.074-.811V3.615a.348.348 0 0 1 .369-.369h2.213a.348.348 0 0 1 .369.369z'
};
const IconTransform = {
  expand : 'rotate(-90 8 8)'
};

IconSVG.propTypes = {
  icon: PropTypes.string.isRequired,
	color: PropTypes.string,
	size: PropTypes.number,
	verticalAlign: PropTypes.string,
  enableBackground: PropTypes.string,
  viewBox: PropTypes.string,
  margin: PropTypes.string,
  float: PropTypes.string,

  style: PropTypes.object
}

IconSVG.defaultProps = {
	color: "#000",
	size: 20,
	verticalAlign: "middle",
  enableBackground: "new 0 0 48 48",
  viewBox: "0 0 48 48",
  margin: "0 0 0 0",
  float: "none"
};

function IconSVG (props) {
  let styles = {
    svg: {
      verticalAlign: props.verticalAlign,
      enableBackground: props.enableBackground,
      margin: props.margin,
      float: props.float
    },
    path: {
      fill: props.color || null 
    }
  }
  let transform = null;
  if (props.style) {
    styles.svg = {...styles.svg, ...props.style};
  }
  if (IconTransform[props.icon]) {
    transform = IconTransform[props.icon]
  }
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      x="0px"
      y="0px"
			viewBox={props.viewBox}
      style={styles.svg}
      width={`${props.size}px`}
      height={`${props.size}px`}
      xmlSpace="preserve"
    >
      <path
        style={styles.path}
        d={ICONS[props.icon]}
        transform={transform}
      />
    </svg>
	)
}

export default IconSVG;
